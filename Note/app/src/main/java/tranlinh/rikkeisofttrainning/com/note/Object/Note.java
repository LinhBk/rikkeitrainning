package tranlinh.rikkeisofttrainning.com.note.Object;

import java.io.Serializable;

/**
 * Created by Tran Linh on 8/2/2016.
 * Đối tượng Note
 * id là một số nguyên, được tính bằng giá trị của chuỗi gồm giờ, phút, giây lập ra note.
 * title là chuỗi tiêu đề
 * text là nội dung
 * bgColor là giá trị màu nên cho note
 * wakeTime là thời gian hẹn giờ cho thông báo
 * currentTime là thời gian khi lập note mới, hoặc là thời gian sau khi sửa thành công.
 * imagePath là chuỗi, gồm đường dẫn đến các hình ảnh được chọn, phân cách nhau bởi dấu ","
 */
public class Note implements Serializable{
    private int id;
    private String title;
    private String text;
    private int bgColor;
    private String wakeTime;
    private String currentTime;
    private String imagePath;

    public boolean equals(Object otherObject){
        if(!(otherObject instanceof Note)){
            return false;
        }
        if(!(title.equals(((Note) otherObject).title))){
            return false;
        }
        if(!(text.equals(((Note) otherObject).text))){
            return false;
        }
        if(bgColor != ((Note) otherObject).bgColor){
            return false;
        }
        if(!(wakeTime.equals(((Note) otherObject).wakeTime))){
            return false;
        }
        if(!(currentTime.equals(((Note) otherObject).currentTime))){
            return false;
        }
        return true;
    }
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }
    public int getBgColor() {
        return bgColor;
    }
    public void setBgColor(int bgColor) {
        this.bgColor = bgColor;
    }
    public String getText() {
        return text;
    }
    public void setText(String text){
        this.text = text;
    }
    public String getWakeTime() {
        return wakeTime;
    }
    public void setWakeTime(String wakeTime) {
        this.wakeTime = wakeTime;
    }
    public String getCurrentTime() {
        return currentTime;
    }
    public String getCurrentTimeWithoutYear(){
        String time[] = currentTime.split(" ");
        String daymonth[] = time[0].split("/");
        String result = String.format("%s/%s %s",daymonth[2],daymonth[1],time[1]);
        return result;
    }
    public void setCurrentTime(String currentTime){
        this.currentTime = currentTime;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }
}
