package tranlinh.rikkeisofttrainning.com.note.HandlingEvent;

import android.app.DatePickerDialog;
import android.content.Context;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import tranlinh.rikkeisofttrainning.com.note.Activity.NewNote;
import tranlinh.rikkeisofttrainning.com.note.R;

/**
 * Created by Tran Linh on 8/5/2016.
 */
public class OnClickSpinnerDayItem implements AdapterView.OnItemSelectedListener {

    private Context context;
    private List<String> listData;
    private ArrayAdapter<String> adapter;

    public OnClickSpinnerDayItem(Context context, List<String> list, ArrayAdapter<String> arrayAdapter){
        this.context = context;
        this.listData = list;
        this.adapter = arrayAdapter;
    }
    @Override
    public void onItemSelected(final AdapterView<?> parent, View view, final int position, long id) {

        if (listData.get(position).equals(context.getString(R.string.sp_day_line4))) {

            DatePickerDialog.OnDateSetListener callback = new DatePickerDialog.OnDateSetListener() {

                @Override
                public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                    monthOfYear = monthOfYear +1;
                    String month = String.valueOf(monthOfYear);
                    if(monthOfYear < 10){
                        month = "0"+month;
                    }
                    String day = String.valueOf(dayOfMonth);
                    if(dayOfMonth < 10){
                        day = "0"+day;
                    }
                    String result = String.format("%s/%s/%d", day, month, year);
                    listData.set(position, result);
                    adapter.notifyDataSetChanged();
                }
            };
            String currentDate = new SimpleDateFormat(NewNote.DAY_FORMAT).format(new Date());
            String strArray[] = currentDate.split("/");
            int year = Integer.parseInt(strArray[0]);
            int month = Integer.parseInt(strArray[1])-1;
            int day = Integer.parseInt(strArray[2]);

            DatePickerDialog chooseDateDialog = new DatePickerDialog(context, callback, year, month, day);

            chooseDateDialog.setTitle(NewNote.SPINNER_DAY_TITLE);
            chooseDateDialog.show();
        } else {
            switch (position){
                case 0:
                case 1:
                case 2:{
                    listData.set(3, context.getString(R.string.sp_day_line4));
                    adapter.notifyDataSetChanged();
                    break;
                }
            }

        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}