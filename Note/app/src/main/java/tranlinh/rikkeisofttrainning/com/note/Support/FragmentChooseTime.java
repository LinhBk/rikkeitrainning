package tranlinh.rikkeisofttrainning.com.note.Support;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import tranlinh.rikkeisofttrainning.com.note.Activity.ActivityNote;
import tranlinh.rikkeisofttrainning.com.note.R;

/**
 * Created by Tran Linh on 8/4/2016.
 */
public class FragmentChooseTime extends Fragment {
    private TextView tv_back;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view= inflater.inflate(R.layout.fragment_choose_time, container, false);
        tv_back = (TextView)view.findViewById(R.id.tv_fragment_choose_time);
        tv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivityNote activity = (ActivityNote) getActivity();
                activity.setAlarm(ActivityNote.ALARM_ON);
                view.setVisibility(view.GONE);
            }
        });
        return view;
    }
}
