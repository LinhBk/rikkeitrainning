package tranlinh.rikkeisofttrainning.com.note.Support;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import tranlinh.rikkeisofttrainning.com.note.Activity.AlarmNote;
import tranlinh.rikkeisofttrainning.com.note.R;

/**
 * Created by Tran Linh on 8/18/2016.
 */
public class CustomBottomToolbar2 extends Fragment {
    private ImageButton ibtnPre,ibtnShare,ibtnNext,ibtnDel;
    private View view;
    AlarmNote alarmNote;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view= inflater.inflate(R.layout.custom_old_note_toolbar, container, false);
        findViewById();
        alarmNote = (AlarmNote)getActivity();
        return view;
    }
    private void findViewById(){
        ibtnPre =(ImageButton) view.findViewById(R.id.ibtn_toolbar_previous);
        ibtnShare = (ImageButton)view.findViewById(R.id.ibtn_toolbar_share);
        ibtnNext = (ImageButton)view.findViewById(R.id.ibtn_toolbar_next);
        ibtnDel = (ImageButton)view.findViewById(R.id.ibtn_toolbar_delete);
        handlingEvent();
    }
    private void handlingEvent(){
        ibtnDel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final AlertDialog.Builder confirmDialog = new AlertDialog.Builder(getContext());
                confirmDialog.setTitle(R.string.confirm_dialog_title);
                confirmDialog.setMessage(R.string.confirm_dialog_mess);
                confirmDialog.setPositiveButton(R.string.confirm_dialog_positive_btn, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        alarmNote.delNote();
                    }
                });
                confirmDialog.setNegativeButton(R.string.confirm_dialog_negative_btn, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                confirmDialog.create();
                confirmDialog.show();
            }
        });
        ibtnShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent sendIntent = new Intent();
                String data = String.format("%s\n%s", alarmNote.gettitle(), alarmNote.gettext());
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, data);
                sendIntent.setType("text/plain");
                startActivity(sendIntent);
            }
        });
        ibtnNext.setColorFilter(getResources().getColor(R.color.dark_ic));
        ibtnPre.setColorFilter(getResources().getColor(R.color.dark_ic));
    }
}
