package tranlinh.rikkeisofttrainning.com.note.Support;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import tranlinh.rikkeisofttrainning.com.note.Object.Note;
import tranlinh.rikkeisofttrainning.com.note.R;

/**
 * Created by Tran Linh on 8/2/2016.
 */
public class CustomGridviewAdapter extends BaseAdapter {
    private Context context;
    private LayoutInflater layoutInflater;
    private List<Note> listNote;
    public CustomGridviewAdapter(Context context, List<Note> listNote){
        this.context = context;
        this.listNote = listNote;
        this.layoutInflater = LayoutInflater.from(context);
    }
    @Override
    public int getCount() {
        return listNote.size();
    }

    @Override
    public Object getItem(int position) {
        return listNote.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if(convertView == null){
            convertView = layoutInflater.inflate(R.layout.custom_grid_view_item,null);
            viewHolder = new ViewHolder();
            viewHolder.mtitle = (TextView)convertView.findViewById(R.id.gridview_item_title);
            viewHolder.mtext = (TextView)convertView.findViewById(R.id.gridview_item_text);
            viewHolder.mliLinearLayout = (LinearLayout)convertView.findViewById(R.id.gridview_item_layout);
            viewHolder.mclockic = (ImageView)convertView.findViewById(R.id.gridview_item_clock_ic);
            viewHolder.mtime = (TextView)convertView.findViewById(R.id.gridview_item_time);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder)convertView.getTag();
        }
        Note note = this.listNote.get(listNote.size()-1-position);
        viewHolder.mtitle.setText(note.getTitle());
        viewHolder.mtext.setText(note.getText());
        viewHolder.mliLinearLayout.setBackgroundColor(Color.parseColor(context.getString(note.getBgColor())));
        if(note.getWakeTime().length() !=0 ){
            viewHolder.mclockic.setImageResource(R.drawable.ic_action_alarms_select);
        }
        viewHolder.mtime.setText(note.getCurrentTimeWithoutYear());

        return convertView;
    }
    class ViewHolder{
        TextView mtitle;
        TextView mtext;
        LinearLayout mliLinearLayout;
        ImageView mclockic;
        TextView mtime;
    }
}
