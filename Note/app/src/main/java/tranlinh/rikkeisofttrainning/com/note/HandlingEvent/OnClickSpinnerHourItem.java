package tranlinh.rikkeisofttrainning.com.note.HandlingEvent;

import android.app.TimePickerDialog;
import android.content.Context;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.TimePicker;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import tranlinh.rikkeisofttrainning.com.note.Activity.NewNote;
import tranlinh.rikkeisofttrainning.com.note.R;


/**
 * Created by Tran Linh on 8/5/2016.
 */
public class OnClickSpinnerHourItem implements AdapterView.OnItemSelectedListener {
    private List<String> listData;
    private ArrayAdapter<String> adapter;
    private Context context;
    public OnClickSpinnerHourItem(Context context,List<String> list, ArrayAdapter<String> adapter){
        this.listData = list;
        this.adapter = adapter;
        this.context = context;
    }
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if(listData.get(position).equals(context.getString(R.string.sp_hour_line5))){
            TimePickerDialog.OnTimeSetListener callback = new TimePickerDialog.OnTimeSetListener() {
                @Override
                public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

                    String hour = String.valueOf(hourOfDay);
                    if(hourOfDay <10){
                        hour = "0"+hour;
                    }
                    String min = String.valueOf(minute);
                    if(minute <10){
                        min = "0"+min;
                    }
                    String result = String.format("%s:%s",hour,min);
                    listData.set(4,result);
                    adapter.notifyDataSetChanged();
                }
            };

            String curentTime = new SimpleDateFormat(NewNote.TIME_FORMAT).format(new Date());
            String strArray[] = curentTime.split(":");
            int hour = Integer.parseInt(strArray[0]);
            int min = Integer.parseInt(strArray[1]);

            TimePickerDialog timePickerDialog = new TimePickerDialog(this.context,callback,hour,min,true);
            timePickerDialog.setTitle(NewNote.SPINNER_HOUR_TITLE);
            timePickerDialog.show();
        }
        else {
            switch (position){
                case 0:
                case 1:
                case 2:
                case 3:{
                    listData.set(4,context.getString(R.string.sp_hour_line5));
                    adapter.notifyDataSetChanged();
                }
            }
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

}
