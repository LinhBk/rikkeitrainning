package tranlinh.rikkeisofttrainning.com.note.Support;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;

import java.util.List;

import tranlinh.rikkeisofttrainning.com.note.Activity.ActivityNote;
import tranlinh.rikkeisofttrainning.com.note.Activity.OldNote;
import tranlinh.rikkeisofttrainning.com.note.R;

/**
 * Created by Tran Linh on 8/11/2016.
 */
public class ImageGridviewAdapter extends BaseAdapter {
    private Context context;
    private List<Bitmap> imageList;
    private LayoutInflater layoutInflater;
    public ImageGridviewAdapter(Context context,List<Bitmap> list){
        this.context = context;
        this.imageList = list;
        this.layoutInflater = LayoutInflater.from(context);
    }
    @Override
    public int getCount() {
        return imageList.size();
    }

    @Override
    public Object getItem(int position) {
        return imageList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder;
        if(convertView == null){
            convertView = layoutInflater.inflate(R.layout.custom_image_gridview_item,null);
            viewHolder = new ViewHolder();
            viewHolder.imageItem = (ImageView)convertView.findViewById(R.id.iv_custom_image_gv_item);
            viewHolder.ibtn_deleteImage = (ImageButton)convertView.findViewById(R.id.ibtn_custom_image_gv_delete_image);
            convertView.setTag(viewHolder);
        }
        else {
            viewHolder = (ViewHolder)convertView.getTag();
        }
        Bitmap image = imageList.get(position);
        viewHolder.imageItem.setImageBitmap(Bitmap.createScaledBitmap(image, 150, 150, false));
        viewHolder.ibtn_deleteImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final AlertDialog.Builder confirmDialog = new AlertDialog.Builder(context);
                confirmDialog.setTitle(context.getString(R.string.confirm_dialog_title));
                confirmDialog.setMessage(context.getString(R.string.confirm_dialog_mess));
                confirmDialog.setPositiveButton(context.getString(R.string.confirm_dialog_positive_btn), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if(context instanceof OldNote){
                            OldNote oldNote = (OldNote) context;
                            oldNote.setImageListHasChange(true);
                        }
                        if(context instanceof ActivityNote){
                            ActivityNote activityNote = (ActivityNote)context;
                            activityNote.removePath(position);
                        }
                        imageList.remove(position);
                        notifyDataSetChanged();
                    }
                });
                confirmDialog.setNegativeButton(context.getString(R.string.confirm_dialog_negative_btn), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                confirmDialog.create();
                confirmDialog.show();
            }
        });
        return convertView;
    }
    class ViewHolder{
        ImageView imageItem;
        ImageButton ibtn_deleteImage;
    }
}
