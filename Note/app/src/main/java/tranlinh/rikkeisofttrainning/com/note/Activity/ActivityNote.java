package tranlinh.rikkeisofttrainning.com.note.Activity;

import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import tranlinh.rikkeisofttrainning.com.note.HandlingEvent.OnClickSpinnerDayItem;
import tranlinh.rikkeisofttrainning.com.note.HandlingEvent.OnClickSpinnerHourItem;
import tranlinh.rikkeisofttrainning.com.note.Object.Note;
import tranlinh.rikkeisofttrainning.com.note.R;
import tranlinh.rikkeisofttrainning.com.note.Support.AdapterForSpinner;
import tranlinh.rikkeisofttrainning.com.note.Support.AlarmReceiver;
import tranlinh.rikkeisofttrainning.com.note.Support.ExpandableHeightGridview;
import tranlinh.rikkeisofttrainning.com.note.Support.FragmentChooseTime;
import tranlinh.rikkeisofttrainning.com.note.Support.ImageGridviewAdapter;

/**
 * Created by Tran Linh on 8/10/2016.
 */
public class ActivityNote extends AppCompatActivity {
    public static final String SPINNER_DAY_TITLE ="Choose Day";
    public static final String SPINNER_HOUR_TITLE ="Choose Time";
    public static final String TIME_FORMAT = "HH:mm";
    public static final String DAY_FORMAT = "yyyy/MM/dd";
    public static final String KEY_FLAG = "key_flag";
    public static final String KEY = "note_back";
    public static final String HAVE_DATA = "have_data";
    public static final String NO_DATA = "no_data";
    public static final String DEL = "del";
    public static final String IMAGE_BITMAP_KEY = "data";
    public static final String POSITION ="position";
    public static final String LIST_SIZE ="list size";
    public static final String DATA = "data";
    public static final int REMOVE = 1995;
    public static final int ALARM_ON =1;
    public static final int ALARM_OFF =0;
    public static final int REQUEST_IMAGE_CAPTURE = 1000;
    public static final int REQUEST_IMAGE_PICK =200;

    protected static final String UNTITLE = "Untitle";
    protected ExpandableHeightGridview gv_image;
    protected ImageButton ibtn_camera,ibtn_change_bg,ibtn_accept,ibtn_close;
    protected EditText ed_title,ed_text;
    protected Spinner sp_day,sp_hour;
    protected TextView tv_time,tv_title,tv_app;
    protected LinearLayout linearLayout;
    protected FrameLayout frameLayout;
    protected FragmentManager manager;
    protected List<String> dayList = new ArrayList<String>();
    protected List<String>hourList = new ArrayList<String>();
    protected List<String> imagePathList = new ArrayList<String>();
    protected List<Bitmap> imageList;
    protected ArrayAdapter<String> dayAddapter,hourAddapter;
    protected ImageGridviewAdapter imageGridviewAdapter;
    protected AdapterForSpinner adapter;
    protected int curBgcolor = R.color.colorWhite;
    protected int alarm =0;
    protected String alarmTime = "";
    protected PendingIntent alarmIntent;
    protected AlarmManager alarmManager;
    protected Intent launchIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    protected void resetData(){
        this.curBgcolor = R.color.colorWhite;
        setAlarm(ALARM_OFF);
        alarmTime ="";
    }
    protected void setupData(Note note){
        ed_title.setText(note.getTitle());
        ed_text.setText(note.getText());
        tv_time.setText(note.getCurrentTime());
        setBackgroundColor(note.getBgColor());
        if(note.getWakeTime().length()==0){
            frameLayout.setVisibility(View.VISIBLE);
            startFragment();
            setupSpinner("","");
        }
        else {
            setAlarm(1);
            frameLayout.setVisibility(View.INVISIBLE);
            String str[] = note.getWakeTime().split(" ");
            String day = str[0];
            String hour = str[1];

            setupSpinner(day, hour);
            sp_day.setSelection(3);
            sp_hour.setSelection(4);
        }
    }

    /**
     * sử dụng trong oldNote và alarmNote
     * lấy ra tập các đường dẫn đến ảnh kèm trong note
     * thêm vào gridView để hiển thị ra.
     * @param note
     */
    protected void setupAllImage(Note note){
        while(!imageList.isEmpty()){
            imageList.remove(0);
            imageGridviewAdapter.notifyDataSetChanged();
            imagePathList.remove(0);
        }
        if(note.getImagePath().length() == 0){
            return;
        }
        String pathList[] = (note.getImagePath()).split(", ");
        for(int i =0; i < pathList.length;i++){
            if(pathList[i].startsWith("/storage")){
                File imgFile = new  File(pathList[i]);
                if(imgFile.exists()){
                    Bitmap bitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                    imageList.add(bitmap);
                    imageGridviewAdapter.notifyDataSetChanged();
                    imagePathList.add(pathList[i]);
                }
            } else{
                if (pathList[i].startsWith("content:")){
                    Uri uri = Uri.parse(pathList[i]);
                    onPickImageResult(uri);
                }
            }
        }

    }
    /**
     * Xử lý kết quả, và trả về cho MainActivity
     */
    public void finishAndSendbackData(){
    }

    /**
     * Tạo ra intent, chứa kết quả trả về cho MainActivity
     * @return
     */
    protected Intent createDataContainer(){
        Intent result = new Intent();
        Note note = new Note();
        if(ed_text.getText().length() == 0 && ed_title.getText().length() == 0 && alarm == 0 && imageList.isEmpty()){
            result.putExtra(KEY_FLAG, NO_DATA);
            return result;
        }
        result.putExtra(KEY_FLAG, HAVE_DATA);

        if((String.valueOf(ed_title.getText())).trim().isEmpty()){
            if((String.valueOf(ed_text.getText())).trim().isEmpty()){
                note.setTitle(UNTITLE);
            }
            else{
                note.setTitle(String.valueOf(ed_text.getText()));
            }
        }
        else{
            note.setTitle(String.valueOf(ed_title.getText()));
        }
        if((String.valueOf(ed_text.getText())).trim().isEmpty()){
            note.setText("");
        }
        else{
            note.setText(String.valueOf(ed_text.getText()));
        }
        note.setCurrentTime(String.valueOf(tv_time.getText()));
        note.setBgColor(curBgcolor);
        if (alarm != 1) {
            alarmTime = "";
        }
        else {
            setAlarmTime();

        }
        note.setWakeTime(alarmTime);
        note.setId(Integer.parseInt(getIdForNote()));
        note.setImagePath(this.imagePathListToString());
        result.putExtra(KEY, note);
        return result;
    }
    public String getTime(int flag){
        String currentDate = new SimpleDateFormat(DAY_FORMAT).format(new Date());
        String currentHour = new SimpleDateFormat(TIME_FORMAT).format(new Date());

        if(flag ==1){
            String str[] = currentDate.split("/");
            currentDate = String.format("%s/%s/%s",str[2],str[1],str[0]);
        }
        return currentDate + " " + currentHour;
    }
    protected String getIdForNote(){
        String currentHour = new SimpleDateFormat("HH:mm:ss:SS").format(new Date());
        String hour[] = currentHour.split(":");
        return hour[0]+hour[1]+hour[2]+hour[3];
    }

    /**
     * Đổi các giá trị Today,Tomorrow... sang ngày thật, và gán lại cho alarmTime.
     */
    protected void setAlarmTime(){
        int daySelected = sp_day.getSelectedItemPosition();
        int hourSelected = sp_hour.getSelectedItemPosition();
        Calendar calendar = Calendar.getInstance();
        Date today = calendar.getTime();

        calendar.add(Calendar.DAY_OF_YEAR, 1);
        Date tomorrow = calendar.getTime();

        calendar.add(Calendar.DAY_OF_YEAR, 6);
        Date nextWeek = calendar.getTime();

        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

        if(dayList.get(daySelected).equals(getString(R.string.sp_day_line1))){
            alarmTime = dateFormat.format(today);
        }
        else {
            if(dayList.get(daySelected).equals(getString(R.string.sp_day_line2))){
                alarmTime = dateFormat.format(tomorrow);
            }else{
                if(dayList.get(daySelected).contains(getString(R.string.sp_day_line3))){
                    alarmTime = dateFormat.format(nextWeek);
                }
                else {
                    alarmTime = dayList.get(daySelected);
                }
            }
        }
        alarmTime = alarmTime + " " + hourList.get(hourSelected);
    }
    protected void setupSpinner(String day,String hour){
        adapter = new AdapterForSpinner(ActivityNote.this,day,hour);
        dayList = adapter.dayListData();
        dayAddapter = adapter.setupAdapter(dayList);
        sp_day.setAdapter(dayAddapter);
        hourList = adapter.hourListData();
        hourAddapter = adapter.setupAdapter(hourList);
        sp_hour.setAdapter(hourAddapter);
        sp_day.setOnItemSelectedListener(new OnClickSpinnerDayItem(ActivityNote.this, dayList, dayAddapter));
        sp_hour.setOnItemSelectedListener(new OnClickSpinnerHourItem(ActivityNote.this, hourList, hourAddapter));
    }
    protected void startFragment(){
        FragmentTransaction transaction= manager.beginTransaction();
        transaction.add(R.id.container, new FragmentChooseTime(), "myFragment");
        transaction.addToBackStack(null);
        transaction.commit();
    }
    protected void findViewById(){
        tv_time = (TextView)findViewById(R.id.tv_newnote_ac_time);
        ibtn_close = (ImageButton)findViewById(R.id.ibtn_newnote_ac_back);
        ibtn_camera = (ImageButton)findViewById(R.id.ibtn_custom_newnote_actionbar_camera);
        sp_day = (Spinner)findViewById(R.id.sp_newnote_ac_day);
        sp_hour = (Spinner)findViewById(R.id.sp_newnote_ac_hour);
        linearLayout = (LinearLayout)findViewById(R.id.lo_newnote);
        frameLayout = (FrameLayout)findViewById(R.id.container);
        ed_title = (EditText)findViewById(R.id.ed_newnote_ac_title);
        ed_text = (EditText) findViewById(R.id.ed_newnote_ac_text);
        gv_image = (ExpandableHeightGridview)findViewById(R.id.gv_newnote_ac_image);

        this.handlingEvent();
    }
    protected void handlingEvent(){
        ed_title.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (ed_title.getText().length() == 0) {
                    tv_title.setText(getString(R.string.app_name));
                } else {
                    tv_title.setText(ed_title.getText());
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        tv_time.setText(getTime(1));
        ibtn_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setAlarm(ALARM_OFF);
                frameLayout.setVisibility(View.VISIBLE);
                startFragment();
            }
        });
        ibtn_camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder cameraDialog = new AlertDialog.Builder(ActivityNote.this);
                cameraDialog.setTitle(R.string.gv_image_title);
                cameraDialog.setItems(R.array.gv_image_items, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0: {
                                dispatchTakePictureIntent();
                                break;
                            }
                            case 1: {
                                dispatchChoosePictureIntent();
                                break;
                            }
                        }
                    }
                });
                cameraDialog.create();
                cameraDialog.show();

            }
        });
    }
    protected String imagePathListToString(){
        String str = imagePathList.toString();
        return str.substring(1,str.length()-1);
    }
    public void removePath(int position){
        imagePathList.remove(position);
    }
    /**
     * chụp ảnh
     */
    protected void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        }
    }

    /**
     * chọn ảnh từ bộ nhớ
     */
    protected void dispatchChoosePictureIntent(){
        Intent intent = new Intent();

        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), REQUEST_IMAGE_PICK);
    }

    /**
     * xử lý kết quả trả về sau khi chụp và chọn ảnh.
     * sau khi chụp ảnh, lưu trữ lại trong bộ nhớ
     * kết quả trả về dưới dạng bitmap, gán vào imageList, rồi hiển thị.
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == ActivityNote.REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            onCaptureImageResult(data);
        }
        if(requestCode == ActivityNote.REQUEST_IMAGE_PICK && resultCode == RESULT_OK && data!=null && data.getData() !=null){
            Uri uri = data.getData();
            onPickImageResult(uri);
        }
    }
    protected void onPickImageResult(Uri uri){
        try {
            Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
            imagePathList.add(String.valueOf(uri));
            imageList.add(bitmap);
            imageGridviewAdapter.notifyDataSetChanged();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    private void onCaptureImageResult(Intent data) {
        Bitmap thumbnail = (Bitmap) data.getExtras().get(IMAGE_BITMAP_KEY);
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        try {
            thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
            File destination = new File(Environment.getExternalStorageDirectory(),
                    System.currentTimeMillis() + ".jpg");

            FileOutputStream fo;
            try {
                destination.createNewFile();
                fo = new FileOutputStream(destination);
                FileInputStream fi = new FileInputStream(destination);
                byte[] image = new byte[fi.available()];
                fo.write(bytes.toByteArray());
                fo.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            imagePathList.add(destination.getPath());
            imageList.add(thumbnail);
            imageGridviewAdapter.notifyDataSetChanged();
        }
        catch(NullPointerException e){
            return;
        }
        catch (Exception e){
            return;
        }

    }

    protected void createCustomActionbar(){
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowHomeEnabled(false);
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayShowTitleEnabled(false);
        View customView = getLayoutInflater().inflate(R.layout.custom_new_note_actionbar, null);
        actionBar.setCustomView(customView);
        Toolbar parent =(Toolbar) customView.getParent();
        parent.setContentInsetsAbsolute(0,20);

        View view = getSupportActionBar().getCustomView();

        tv_app = (TextView)view.findViewById(R.id.tv_custom_newnote_actionbar_app);
        ibtn_camera = (ImageButton)view.findViewById(R.id.ibtn_custom_newnote_actionbar_camera);
        ibtn_change_bg = (ImageButton)view.findViewById(R.id.ibtn_custom_newnote_actionbar_change_bg);
        ibtn_accept = (ImageButton) view.findViewById(R.id.ibtn_custom_newnote_actionbar_accept);
        tv_title = (TextView)view.findViewById(R.id.tv_custom_newnote_actionbar_title);

        tv_app.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        ibtn_change_bg.setOnClickListener(new ShowDialogChooseColor(ActivityNote.this));
        ibtn_accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finishAndSendbackData();
            }
        });
    }
    protected void createCustomToolbar(){}
    protected void setBackgroundColor(int color){
        curBgcolor = color;
        linearLayout.setBackgroundColor(Color.parseColor(getString(color)));
        frameLayout.setBackgroundColor(Color.parseColor(getString(color)));
    }
    public void setAlarm(int alarm) {
        this.alarm = alarm;
    }
    protected  class ShowDialogChooseColor implements View.OnClickListener {
        private Context context;
        private ImageButton ibtnWhite,ibtnOrange,ibtnBlue,ibtnGreen;
        private Dialog dialogChooseColor;
        public ShowDialogChooseColor(Context context){
            this.context = context;
        }
        @Override
        public void onClick(View v) {
            dialogChooseColor = new Dialog(context);
            dialogChooseColor.setContentView(R.layout.dialog_choose_bg_color);
            dialogChooseColor.setTitle(context.getString(R.string.dialog_choose_bg_color_title));

            ibtnWhite = (ImageButton)dialogChooseColor.findViewById(R.id.ibtn_dialog_white);
            ibtnWhite.setBackgroundColor(getResources().getColor(R.color.colorWhite));
            ibtnBlue = (ImageButton)dialogChooseColor.findViewById(R.id.ibtn_dialog_blue);
            ibtnGreen = (ImageButton)dialogChooseColor.findViewById(R.id.ibtn_dialog_green);
            ibtnOrange = (ImageButton)dialogChooseColor.findViewById(R.id.ibtn_dialog_orange);

            ibtnBlue.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    changeColor(R.color.colorBlue);
                }
            });
            ibtnWhite.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    changeColor(R.color.colorWhite);
                }
            });
            ibtnGreen.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    changeColor(R.color.colorGreen);
                }
            });
            ibtnOrange.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    changeColor(R.color.colorOrange);
                }
            });
            dialogChooseColor.show();
        }
        private void changeColor(int color){
            setBackgroundColor(color);
            dialogChooseColor.cancel();
        }

    }
    protected void createImageGridView(){
        imageList = new ArrayList<Bitmap>();
        imageGridviewAdapter = new ImageGridviewAdapter(this,imageList);
        gv_image.setExpand(true);
        gv_image.setAdapter(imageGridviewAdapter);
    }
    protected void startAlarmNotification(String arg0){
        String str[] = arg0.split(" ");
        String day = str[0];
        String hour = str[1];
        String str1[] = day.split("/");
        String str2[] = hour.split(":");

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        long tmp1 = calendar.getTimeInMillis();
        calendar.set(Calendar.YEAR, Integer.parseInt(str1[2]));
        calendar.set(Calendar.MONTH,Integer.parseInt(str1[1])-1);
        calendar.set(Calendar.DAY_OF_MONTH, Integer.parseInt(str1[0]));
        calendar.set(Calendar.HOUR_OF_DAY, Integer.parseInt(str2[0]));
        calendar.set(Calendar.MINUTE, Integer.parseInt(str2[1]));
        calendar.set(Calendar.SECOND, 0);
        long tmp2 = calendar.getTimeInMillis();
        if(tmp1>tmp2){
            return;
        }
        alarmManager.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), alarmIntent);
    }
    protected void stopAlarmNotification(){
        alarmManager.cancel(alarmIntent);
    }
    protected void setAlarmNotificationOnOff(Note note){
        launchIntent = new Intent(this,AlarmReceiver.class);
        alarmManager = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
        if(alarm!=1){
            alarmIntent = PendingIntent.getBroadcast(this, note.getId(), launchIntent, PendingIntent.FLAG_UPDATE_CURRENT);
            stopAlarmNotification();
        }
        else{
            launchIntent.putExtra(ActivityNote.KEY,note);
            alarmIntent = PendingIntent.getBroadcast(this,note.getId(),launchIntent,PendingIntent.FLAG_UPDATE_CURRENT);
            startAlarmNotification(alarmTime);
        }
    }

    /**
     * xử lý nut back, quay trở về MainActivity
     * @param keyCode
     * @param event
     * @return
     */
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            finish();
            return true;
        }

        return super.onKeyDown(keyCode, event);
    }
}
