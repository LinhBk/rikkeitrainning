package tranlinh.rikkeisofttrainning.com.note.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.view.View;

import tranlinh.rikkeisofttrainning.com.note.Object.Note;
import tranlinh.rikkeisofttrainning.com.note.R;
import tranlinh.rikkeisofttrainning.com.note.Support.CustomBottomToolbar2;

/**
 * Created by Tran Linh on 8/18/2016.
 * Hien thi khi an vao notification
 */
public class AlarmNote extends ActivityNote {
    private Note note;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_note);
        manager= getSupportFragmentManager();
        super.createCustomActionbar();
        super.findViewById();
        super.createImageGridView();
        setupData();
    }
    private void setupData(){
        Intent intent = getIntent();
        note= (Note)intent.getSerializableExtra(ActivityNote.KEY);
        if(note == null){
            return;
        }
        super.setupData(note);
        this.createCustomToolbar();
        ibtn_change_bg.setVisibility(View.INVISIBLE);
        ibtn_camera.setVisibility(View.INVISIBLE);
        ibtn_accept.setVisibility(View.INVISIBLE);
        tv_app.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finishAndSendbackData();
            }
        });
        setupAllImage(note);
    }
    @Override
    public void finishAndSendbackData() {
        Intent data = createDataContainer();
        Intent intent = new Intent(this,MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        if(data.getStringExtra(KEY_FLAG).equals(NO_DATA)){
            intent.putExtra(KEY_FLAG,NO_DATA);
            startActivity(intent);
            return;
        }
        intent.putExtra(KEY_FLAG,HAVE_DATA);
        Note noteBack = (Note)data.getSerializableExtra(ActivityNote.KEY);
        noteBack.setId(note.getId());
        if(!noteBack.equals(note)){
            noteBack.setCurrentTime(getTime(2));
            setAlarmNotificationOnOff(noteBack);
        }
        intent.putExtra(KEY, noteBack);
        startActivity(intent);
    }
    @Override
    protected void createCustomToolbar(){
        FragmentTransaction transaction= manager.beginTransaction();
        transaction.add(R.id.bottom_toolbar, new CustomBottomToolbar2(), "bottom toolbar 2");
        transaction.addToBackStack(null);
        transaction.commit();
    }
    public void delNote(){
        Intent data = new Intent(this,MainActivity.class);
        data.putExtra(ActivityNote.KEY, note);
        data.putExtra(ActivityNote.KEY_FLAG,ActivityNote.DEL);
        data.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(data);
    }
    public String gettitle(){
        return String.valueOf(ed_title.getText());
    }
    public String gettext(){
        return String.valueOf(ed_text.getText());
    }
}
