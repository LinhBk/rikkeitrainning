package tranlinh.rikkeisofttrainning.com.note.Activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import tranlinh.rikkeisofttrainning.com.note.Object.Note;
import tranlinh.rikkeisofttrainning.com.note.R;

/**
 * Created by Tran Linh on 8/3/2016.
 * Hien thi khi them 1 note moi
 */
public class NewNote extends ActivityNote {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_note);
        super.resetData();
        super.createCustomActionbar();
        super.findViewById();
        manager= getSupportFragmentManager();
        super.startFragment();
        super.setBackgroundColor(R.color.colorWhite);
        super.setupSpinner("","");
        super.createImageGridView();
    }


    @Override
    public void finishAndSendbackData(){
        Intent data = createDataContainer();
        if(data.getStringExtra(KEY_FLAG) == HAVE_DATA){
            Note note = (Note)data.getSerializableExtra(KEY);
            note.setCurrentTime(getTime(2));
            data.putExtra(KEY, note);
            setAlarmNotificationOnOff(note);
        }
        this.setResult(Activity.RESULT_OK, data);

        super.finish();
    }
}
