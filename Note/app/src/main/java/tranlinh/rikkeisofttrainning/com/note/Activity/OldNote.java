package tranlinh.rikkeisofttrainning.com.note.Activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;

import java.util.ArrayList;
import java.util.List;

import tranlinh.rikkeisofttrainning.com.note.Object.Note;
import tranlinh.rikkeisofttrainning.com.note.R;
import tranlinh.rikkeisofttrainning.com.note.Support.CustomBottomToolbar;

/**
 * Created by Tran Linh on 8/7/2016.
 * Hiển thị khi xem lại 1 note cũ.
 */
public class OldNote extends ActivityNote {
    private int position=0;
    private List<Note> list;
    private int size;
    private Note note;
    private boolean imageListHasChange = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_note);
        manager= getSupportFragmentManager();
        super.createCustomActionbar();
        super.findViewById();
        super.createImageGridView();
        getData();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(data != null){
            setImageListHasChange(true);
        }
        super.onActivityResult(requestCode, resultCode, data);

    }

    public void setupData(int mposition){
        note = list.get(mposition);
        super.setupData(note);
        setImageListHasChange(false);
        setupAllImage(note);
        createCustomToolbar();
    }
    private void getData(){
        Intent intent = getIntent();
        list = (ArrayList<Note>)intent.getSerializableExtra(ActivityNote.DATA);
        position = intent.getIntExtra(ActivityNote.POSITION,0);
        size = intent.getIntExtra(ActivityNote.LIST_SIZE, 0);
        setupData(position);
    }
    @Override
    protected void createCustomToolbar(){
        FragmentTransaction transaction= manager.beginTransaction();
        transaction.add(R.id.bottom_toolbar, new CustomBottomToolbar(), "bottom toolbar");
        transaction.addToBackStack(null);
        transaction.commit();
    }
    @Override
    public void finishAndSendbackData(){
        Intent data = createDataContainer();
        if(data.getStringExtra(KEY_FLAG).equals(NO_DATA)){
            this.setResult(Activity.RESULT_OK, data);
            super.finish();
            return;
        }
        Note noteSendBack = (Note)data.getSerializableExtra(KEY);
        noteSendBack.setId(note.getId());
        if(!noteSendBack.equals(list.get(position)) || imageListHasChange){
            noteSendBack.setCurrentTime(getTime(2));
            setAlarmNotificationOnOff(noteSendBack);
        }
        data.putExtra(KEY,noteSendBack);
        data.putExtra(ActivityNote.POSITION, position);
        this.setResult(Activity.RESULT_OK, data);
        super.finish();
    }
    public void delNote(){
        Intent data = new Intent();
        data.putExtra(ActivityNote.KEY,note);
        data.putExtra(ActivityNote.POSITION, position);
        this.setResult(ActivityNote.REMOVE, data);
        super.finish();
    }
    public String gettitle(){
        return String.valueOf(ed_title.getText());
    }
    public String gettext(){
        return String.valueOf(ed_text.getText());
    }
    public void setPosition(int argv){
        this.position += argv;
    }
    public int getPosition(){
        return this.position;
    }
    public int getSize() {
        return size;
    }
    public void setImageListHasChange(Boolean value){
        this.imageListHasChange = value;
    }
}
