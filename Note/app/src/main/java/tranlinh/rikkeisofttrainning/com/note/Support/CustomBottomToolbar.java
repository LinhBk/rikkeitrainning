package tranlinh.rikkeisofttrainning.com.note.Support;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import tranlinh.rikkeisofttrainning.com.note.Activity.OldNote;
import tranlinh.rikkeisofttrainning.com.note.R;

/**
 * Created by Tran Linh on 8/10/2016.
 */
public class CustomBottomToolbar extends Fragment {
    private ImageButton ibtnPre,ibtnShare,ibtnNext,ibtnDel;
    private OldNote oldNote;
    private View view;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view= inflater.inflate(R.layout.custom_old_note_toolbar, container, false);
        oldNote = (OldNote) getActivity();
        findViewById();
        hightlightIc();
        return view;
    }
    private void findViewById(){
        ibtnPre =(ImageButton) view.findViewById(R.id.ibtn_toolbar_previous);
        ibtnShare = (ImageButton)view.findViewById(R.id.ibtn_toolbar_share);
        ibtnNext = (ImageButton)view.findViewById(R.id.ibtn_toolbar_next);
        ibtnDel = (ImageButton)view.findViewById(R.id.ibtn_toolbar_delete);
        handlingEvent();
    }
    private void handlingEvent(){
        ibtnDel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final AlertDialog.Builder confirmDialog = new AlertDialog.Builder(getContext());
                confirmDialog.setTitle(R.string.confirm_dialog_title);
                confirmDialog.setMessage(R.string.confirm_dialog_mess);
                confirmDialog.setPositiveButton(R.string.confirm_dialog_positive_btn, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        oldNote.delNote();
                    }
                });
                confirmDialog.setNegativeButton(R.string.confirm_dialog_negative_btn, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                confirmDialog.create();
                confirmDialog.show();
            }
        });
        ibtnShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent sendIntent = new Intent();
                String data = String.format("%s\n%s",oldNote.gettitle(),oldNote.gettext());
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, data);
                sendIntent.setType("text/plain");
                startActivity(sendIntent);
            }
        });
        ibtnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int mposition = position();
                if(mposition != 1 && mposition != 3){
                    oldNote.setPosition(-1);
                    oldNote.setupData(oldNote.getPosition());
                    hightlightIc();
                }
            }
        });
        ibtnPre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int mposition = position();
                if(mposition != 0 && mposition !=3){
                    oldNote.setPosition(1);
                    oldNote.setupData(oldNote.getPosition());
                    hightlightIc();
                }
            }
        });
    }
    private int position(){

        int size = oldNote.getSize();
        int mposition = oldNote.getPosition();
        if(size ==1){
            return 3;
        }
        if(size == mposition+1){
            return 0;
        }
        if(mposition == 0 ){
            return 1;
        }
        return 2;
    }

    private void hightlightIc(){
        int mposition = position();
        switch (mposition){
            case 0: {
                ibtnPre.setColorFilter(getResources().getColor(R.color.dark_ic));
                ibtnNext.setColorFilter(getResources().getColor(R.color.colorWhite));
                break;
            }
            case 1:{
                ibtnNext.setColorFilter(getResources().getColor(R.color.dark_ic));
                ibtnPre.setColorFilter(getResources().getColor(R.color.colorWhite));
                break;
            }
            case 2:{
                ibtnPre.setColorFilter(getResources().getColor(R.color.colorWhite));
                ibtnNext.setColorFilter(getResources().getColor(R.color.colorWhite));
                break;
            }
            case 3:{
                ibtnPre.setColorFilter(getResources().getColor(R.color.dark_ic));
                ibtnNext.setColorFilter(getResources().getColor(R.color.dark_ic));
                break;
            }

        }
    }
}
