package tranlinh.rikkeisofttrainning.com.note.Support;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.NotificationCompat;

import tranlinh.rikkeisofttrainning.com.note.Activity.ActivityNote;
import tranlinh.rikkeisofttrainning.com.note.Activity.AlarmNote;
import tranlinh.rikkeisofttrainning.com.note.Activity.MainActivity;
import tranlinh.rikkeisofttrainning.com.note.Object.Note;
import tranlinh.rikkeisofttrainning.com.note.R;

/**
 * Created by Tran Linh on 8/17/2016.
 */
public class AlarmReceiver extends BroadcastReceiver {
    private NotificationCompat.Builder notBuilder;
    private static final String title = "Note notification";
    private Note note;
    @Override
    public void onReceive(Context context, Intent intent) {
        note =(Note) intent.getSerializableExtra(ActivityNote.KEY);

        this.notBuilder = new NotificationCompat.Builder(context);
        this.notBuilder.setAutoCancel(true);

        this.notBuilder.setSmallIcon(R.drawable.ic_launcher);
        this.notBuilder.setTicker(this.title);
        this.notBuilder.setWhen(System.currentTimeMillis() + 10 * 1000);
        this.notBuilder.setContentTitle(this.title);
        this.notBuilder.setContentText(note.getTitle());

        intent = new Intent(context, AlarmNote.class);
        intent.putExtra(ActivityNote.KEY,note);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, MainActivity.REQUEST_OLD_NOTE,
                intent, PendingIntent.FLAG_UPDATE_CURRENT);

        this.notBuilder.setContentIntent(pendingIntent);

        NotificationManager notificationService  =
                (NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);

        Notification notification =  notBuilder.build();
        notificationService.notify(note.getId(), notification);

    }
}
