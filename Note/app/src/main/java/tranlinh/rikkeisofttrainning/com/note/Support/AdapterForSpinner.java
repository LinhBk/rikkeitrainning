package tranlinh.rikkeisofttrainning.com.note.Support;

import android.content.Context;
import android.widget.ArrayAdapter;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import tranlinh.rikkeisofttrainning.com.note.R;

/**
 * Created by Tran Linh on 8/5/2016.
 */
public class AdapterForSpinner {
    private Context context;
    private String day,hour;
    public AdapterForSpinner(Context context,String day,String hour){
        this.context = context;
        this.day = day;
        this.hour = hour;
    }
    public ArrayAdapter<String> setupAdapter(List<String> listData){
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(context,android.R.layout.simple_spinner_item,listData);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        return adapter;
    }
    public List<String> dayListData(){
        List<String> list = new ArrayList<String>();
        list.add(context.getString(R.string.sp_day_line1));
        list.add(context.getString(R.string.sp_day_line2));
        Calendar c = Calendar.getInstance();
        Date date = c.getTime();
        DateFormat dateFormat = new SimpleDateFormat("EEEE");
        list.add(String.format(context.getString(R.string.sp_day_line3_template), dateFormat.format(date)));
        if(day.length()==0){
            list.add(context.getString(R.string.sp_day_line4));
        }
        else {
            list.add(day);
        }
        return list;
    }
    public List<String> hourListData(){
        List<String> list = new ArrayList<>();
        list.add(context.getString(R.string.sp_hour_line1));
        list.add(context.getString(R.string.sp_hour_line2));
        list.add(context.getString(R.string.sp_hour_line3));
        list.add(context.getString(R.string.sp_hour_line4));
        if(hour.length()==0){
            list.add(context.getString(R.string.sp_hour_line5));
        }
        else {
            list.add(this.hour);
        }
        return list;
    }
}
