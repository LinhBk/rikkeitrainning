package tranlinh.rikkeisofttrainning.com.note.Support;

import android.content.Context;
import android.util.AttributeSet;
import android.view.ViewGroup;
import android.widget.GridView;

/**
 * Created by Tran Linh on 8/11/2016.
 */
public class ExpandableHeightGridview extends GridView {
    private boolean expand = false;
    public ExpandableHeightGridview(Context context) {
        super(context);
    }

    public ExpandableHeightGridview(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ExpandableHeightGridview(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }
    public boolean isExpand(){
        return expand;
    }
    public void setExpand(boolean expand){
        this.expand = expand;
    }
    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        if(isExpand()){
            int expandSpec = MeasureSpec.makeMeasureSpec(MEASURED_SIZE_MASK,
                    MeasureSpec.AT_MOST);
            super.onMeasure(widthMeasureSpec, expandSpec);

            ViewGroup.LayoutParams params = getLayoutParams();
            params.height = getMeasuredHeight();
        }
        else{
            super.onMeasure(heightMeasureSpec, heightMeasureSpec);
        }
    }
}
