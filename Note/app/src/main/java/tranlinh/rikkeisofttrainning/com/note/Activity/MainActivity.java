package tranlinh.rikkeisofttrainning.com.note.Activity;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import tranlinh.rikkeisofttrainning.com.note.Object.Note;
import tranlinh.rikkeisofttrainning.com.note.R;
import tranlinh.rikkeisofttrainning.com.note.Support.CustomGridviewAdapter;

/**
 * MainActivity
 * Hiển thị layout chính activity_main
 * Khi bắt đầu (onCreate) thì lấy dữ liệu từ trong database ( nếu có), hoặc dữ liệu từ intent gửi tới ( sau khi hiển thị AlarmNote) để
 * để thêm vào noteList.
 * Khi dừng lại (onStop), lưu trữ lại dữ liệu trong noteList vào database.
 * Bắt đầu activity khác (NewNote, OldNote) và nhận về kết quả là Note để thay đổi trong noteList.
 */

public class MainActivity extends AppCompatActivity {
    public static final int REQUEST_NEW_NOTE = 100;
    public static final int REQUEST_OLD_NOTE = 2;
    public static final String KEY_DATA_SAVED = "key";
    public static final String DB_NAME = "database.db";
    public static final String TABLE = "stored_data";
    public static final String TABLE_COLUMN = "data";
    public static final String TABLE_IMAGE = "image";
    public static final String TB_IMAGE_CO_1 ="id";
    public static final String TB_IMAGE_CO_2 = "stored_image";

    private List<Note> noteList = new ArrayList<Note>();
    private CustomGridviewAdapter adapter;
    private TextView tvNoNote;
    private GridView listOfStoredNote;

    SQLiteDatabase db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.findViewById();
        this.createCustomActionbar();
        this.createGridview();
        this.getStoredData();
        this.getNoteChangedFromPendingIntent();
    }
    @Override
    protected void onStop() {
        super.onStop();
        db.execSQL("delete from " + TABLE);
        Gson gson = new Gson();
        if(noteList.isEmpty()){
            return;
        }
        ContentValues values = new ContentValues();
        for(int i = 0; i < noteList.size(); i++){
            Note note = (Note)noteList.get(i);
            values.put(TABLE_COLUMN, gson.toJson(note).getBytes());
            db.insert(TABLE, null, values);
        }
    }
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable(KEY_DATA_SAVED, (Serializable) noteList);
    }
    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        List<Note>list = (List<Note>)savedInstanceState.getSerializable(KEY_DATA_SAVED);
        if(list.isEmpty()){
            emptyAdapter();
        }else{
            for(int i = 0; i < list.size(); i++ ){
                noteList.add((Note)list.get(i));
            }
            tvNoNote.setText("");
            adapter.notifyDataSetChanged();
            listOfStoredNote.setAdapter(adapter);
        }

    }
    /**
     * lấy ra dư liệu trong intent, gửi đến từ AlarmNote, gán vào cho noteList
     * hoặc xóa đi note có id trùng với id của note gửi về.
     */
    protected void getNoteChangedFromPendingIntent(){
        Intent intent = getIntent();
        if(intent.getSerializableExtra(ActivityNote.KEY) != null){
            String flag = intent.getStringExtra(ActivityNote.KEY_FLAG);
            if(flag.equals(ActivityNote.HAVE_DATA) || flag.equals(ActivityNote.DEL)) {
                Note note = (Note) intent.getSerializableExtra(ActivityNote.KEY);
                for (int i = 0; i < noteList.size(); i++) {
                    if ((noteList.get(i)).getId() == note.getId()) {
                        noteList.remove(i);
                        adapter.notifyDataSetChanged();
                        listOfStoredNote.setAdapter(adapter);
                        emptyAdapter();
                        break;
                    }
                }
                if(flag.equals(ActivityNote.HAVE_DATA)){
                    addNote(note);
                }
            }
        }
    }

    /**
     * lấy ra dữ liệu trong database
     */
    private void getStoredData(){

        db = this.openOrCreateDatabase(DB_NAME, Context.MODE_PRIVATE,null);
        db.execSQL("create table if not exists " + TABLE + " ( " + TABLE_COLUMN + " blob )");
        Cursor cursor = db.rawQuery("select * from "+TABLE, null);
        if(!cursor.moveToFirst()){
            emptyAdapter();
            return;
        }
        do {
            byte[] blob = cursor.getBlob(cursor.getColumnIndex(TABLE_COLUMN));
            String json = new String(blob);
            Gson gson = new Gson();
            Note note = gson.fromJson(json, new TypeToken<Note>() {
            }.getType());
            noteList.add(note);
            adapter.notifyDataSetChanged();
        }while (cursor.moveToNext());
        tvNoNote.setText("");
    }
    private void findViewById(){
        listOfStoredNote = (GridView)findViewById(R.id.gvListOfStoredNote);
        tvNoNote = (TextView)findViewById(R.id.tvNoNote);
    }
    private void createCustomActionbar(){
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setCustomView(R.layout.custom_main_actionbar);

        View view = getSupportActionBar().getCustomView();

        ImageButton imageButton = (ImageButton)view.findViewById(R.id.mainActionbarPlush);
        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent gotoNewNoteActivity = new Intent(MainActivity.this, NewNote.class);

                startActivityForResult(gotoNewNoteActivity, REQUEST_NEW_NOTE);
            }
        });
    }
    private void createGridview(){

        adapter = new CustomGridviewAdapter(this,noteList);

        listOfStoredNote.setAdapter(adapter);
        listOfStoredNote.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent gotoOldNoteActivity = new Intent(MainActivity.this, OldNote.class);
                gotoOldNoteActivity.putExtra(ActivityNote.DATA, (Serializable) noteList);
                gotoOldNoteActivity.putExtra(ActivityNote.POSITION, noteList.size() - 1 - position);
                gotoOldNoteActivity.putExtra(ActivityNote.LIST_SIZE, noteList.size());
                startActivityForResult(gotoOldNoteActivity, REQUEST_OLD_NOTE);
            }
        });
        emptyAdapter();
    }
    private void emptyAdapter(){
        if(adapter.isEmpty()){
            tvNoNote.setText(R.string.main_activity_tv_no_note);
        }
    }
    private void addNote(Note note){
        noteList.add(note);
        adapter.notifyDataSetChanged();
        listOfStoredNote.setAdapter(adapter);
        tvNoNote.setText("");
    }

    /**
     * Hàm xử lý kết quả trả về từ NewNote và OldNote
     * Nếu gửi về từ NewNote, trong intent có một note trống thì bỏ qua, ngược lại, thêm vào noteList, hiển thị ra màn hình.
     * Nếu gửi về từ OldNote, trong intent có một note trống thì bỏ qua, ngược lại, kiểm tra note gửi về có trùng với note đã gửi đi không
     *  nếu có, thì bỏ qua, ngược lại, xóa note đã gửi đi, thêm note mới vào noteList.
     * Nếu resultCode là REMOVE, thì kiểm tra trong noteList, note gửi đi và note hiện ở vị trí gửi đi có giống nhau không, đúng thì xóa.
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (resultCode){
            case Activity.RESULT_OK:{
                switch (requestCode){
                    case MainActivity.REQUEST_NEW_NOTE:{
                        if(data.getStringExtra(NewNote.KEY_FLAG).equals(NewNote.HAVE_DATA)){
                            Note note = (Note)data.getSerializableExtra(ActivityNote.KEY);
                            addNote(note);
                        }
                        break;
                    }
                    case MainActivity.REQUEST_OLD_NOTE:{
                        if(data.getStringExtra(NewNote.KEY_FLAG).equals(NewNote.HAVE_DATA)){
                            Note note = (Note)data.getSerializableExtra(ActivityNote.KEY);
                            int position = data.getIntExtra(ActivityNote.POSITION, 0);
                            if(note.equals(noteList.get(position))){
                                return;
                            }
                            noteList.remove(position);
                            addNote(note);
                        }
                    }
                }
                break;
            }
            case ActivityNote.REMOVE:{
                if(requestCode == REQUEST_OLD_NOTE){
                    int position = data.getIntExtra(ActivityNote.POSITION,0);
                    Note note = (Note)data.getSerializableExtra(ActivityNote.KEY);
                    if(note.equals(noteList.get(position))){
                        noteList.remove(position);
                        adapter.notifyDataSetChanged();
                        listOfStoredNote.setAdapter(adapter);
                        emptyAdapter();
                    }
                }
                break;
            }
        }
    }
}
