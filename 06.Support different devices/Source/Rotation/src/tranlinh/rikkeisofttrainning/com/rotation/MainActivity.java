package tranlinh.rikkeisofttrainning.com.rotation;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    private LinearLayout layout;
    private Button btn;
    private TextView tv;
    private EditText ed;
    private String str;
    @Override
    protected void onSaveInstanceState(Bundle outState){
        super.onSaveInstanceState(outState);
        outState.putString(str,String.valueOf(tv.getText()));
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        String str1 = savedInstanceState.getString(str);
        tv.setText(str1);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.connectWitdId();
        tv = new TextView(this);
        tv.setTextSize(getResources().getDimension(R.dimen.text_size));
        tv.setTextColor(getResources().getColor(R.color.colorAccent));
        layout.addView(tv);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tv.setText(ed.getText());
            }
        });

    }
    private void connectWitdId(){
        layout = (LinearLayout)findViewById(R.id.layout);
        btn = (Button)findViewById(R.id.btn);
        ed = (EditText)findViewById(R.id.edittext1);
    }
}
