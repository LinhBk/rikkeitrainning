package tranlinh.rikkeisofttrainning.com.layout;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.RadioGroup;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    private RadioGroup radioGroup;
    private TextView tv;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.getId();

    }
    private void getId(){
        radioGroup = (RadioGroup)findViewById(R.id.radioGroup);
        tv = (TextView)findViewById(R.id.tv);
    }
    private void setBackgroundColor(int colorId){
        tv.setBackgroundColor(colorId);
    }
    public void setColor(View view){
        int idChecked = radioGroup.getCheckedRadioButtonId();
        switch (idChecked){
            case R.id.rbtnBlue:{
                this.setBackgroundColor(Color.BLUE);
                break;
            }
            case R.id.rbtnRed:{
                this.setBackgroundColor(Color.RED);
                break;
            }
            case R.id.rbtnWhite:{
                this.setBackgroundColor(Color.WHITE);
                break;
            }
        }
    }
    public void clear(View view){
        this.setBackgroundColor(Color.BLACK);
    }
}
