package tranlinh.rikkeisofttrainning.com.tablelayout;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    private TextView tv;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tv = (TextView)findViewById(R.id.tv);
    }
    private void changeTVColor(int colorId){
        tv.setBackgroundColor(colorId);
    }
    public void changeTVColor1(View view){
        this.changeTVColor(Color.RED);
    }
    public void changeTVColor2(View view){
        this.changeTVColor(Color.YELLOW);
    }
    public void changeTVColor3(View view){
        this.changeTVColor(Color.BLUE);
    }
    public void clear(View view){
        this.changeTVColor(Color.WHITE);
    }
}
