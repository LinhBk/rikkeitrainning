package tranlinh.rikkeisofttrainning.com.playsongservice;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
    public void playSound(View view){
        Intent intent = new Intent(MainActivity.this,PlaySongService.class);
        this.startService(intent);
    }
    public void stopSound(View view){
        Intent intent = new Intent(MainActivity.this,PlaySongService.class);
        this.stopService(intent);
    }
}
