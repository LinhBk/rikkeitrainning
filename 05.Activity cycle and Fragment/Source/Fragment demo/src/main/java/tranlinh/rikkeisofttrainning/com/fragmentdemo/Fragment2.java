package tranlinh.rikkeisofttrainning.com.fragmentdemo;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by Tran Linh on 7/14/2016.
 */
public class Fragment2 extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        //---Inflate the layout for this fragment---
        return inflater.inflate(
                R.layout.fragment_2, container, false);
    }
    @Override
    public void onStart(){
        super.onStart();
        Button btn = (Button)getActivity().findViewById(R.id.btn);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextView tv = (TextView)getActivity().findViewById(R.id.tv1);
                Toast.makeText(getActivity(),tv.getText().toString(),Toast.LENGTH_SHORT).show();
            }
        });
    }
}
