package tranlinh.rikkeisofttrainning.com.exerciesbutton1;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

public class MainActivity extends AppCompatActivity {
    private ImageButton ibtnRed,ibtnBlue;
    private TextView tv;
    private RadioGroup radioGroup,radioGroup2;
    private Button btnChose;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.connectWithId();

        ibtnRed.setOnClickListener(new clickOnImageButton(Color.RED));
        ibtnBlue.setOnClickListener(new clickOnImageButton(Color.BLUE));
        radioGroup.setOnCheckedChangeListener(new clickOnRadioButton());
        btnChose.setOnClickListener(new clickOnButton());
    }
    private void connectWithId(){
        ibtnRed = (ImageButton)findViewById(R.id.ibtnRed);
        ibtnBlue = (ImageButton)findViewById(R.id.ibtnBlue);
        tv = (TextView)findViewById(R.id.tv);
        radioGroup = (RadioGroup)findViewById(R.id.radioGroup);
        radioGroup2 =(RadioGroup)findViewById(R.id.radioGroup2);
        btnChose = (Button)findViewById(R.id.btnchose);
    }
    private void changeTextColor(int colorId){
        tv.setTextColor(colorId);
    }
    private void changeBackgroundColor(int colorId){
        tv.setBackgroundColor(colorId);
    }


    private class clickOnImageButton implements View.OnClickListener{
        private int colorId;
        public clickOnImageButton(int colorId){
            this.colorId = colorId;
        }
        @Override
        public void onClick(View v) {
            changeTextColor(colorId);
        }
    }
    private class clickOnRadioButton implements RadioGroup.OnCheckedChangeListener{

        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            switch (checkedId){
                case R.id.rbtnBlue:{
                    changeBackgroundColor(Color.BLUE);
                    break;
                }
                case R.id.rbtnRed:{
                    changeBackgroundColor(Color.RED);
                    break;
                }
                case R.id.rbtnYellow:{
                    changeBackgroundColor(Color.YELLOW);
                    break;
                }
            }
        }
    }
    public void clickOnToggleButton(View view){
        ToggleButton toggleButton = (ToggleButton)view;
        if(toggleButton.isChecked()){
            switch (toggleButton.getId()){
                case R.id.tbBlue:{
                    changeBackgroundColor(Color.BLUE);
                    break;
                }
                case R.id.tbRed:{
                    changeBackgroundColor(Color.RED);
                    break;
                }
                case R.id.tbYellow:{
                    changeBackgroundColor(Color.YELLOW);
                    break;
                }
            }
        }
        else{
            this.changeBackgroundColor(Color.BLACK);
        }

    }
    private class clickOnButton implements View.OnClickListener{

        @Override
        public void onClick(View v) {
            int idChecked = radioGroup2.getCheckedRadioButtonId();
            switch (idChecked){
                case R.id.rbtnBlueLine2:{
                    changeTextColor(Color.BLUE);
                    break;
                }
                case R.id.rbtnRedLine2:{
                    changeTextColor(Color.RED);
                    break;
                }
                case R.id.rbtnYellowLine2:{
                    changeTextColor(Color.YELLOW);
                    break;
                }
                default:{
                    Toast.makeText(MainActivity.this,"You have to check in a radio button first",Toast.LENGTH_SHORT).show();
                }
            }
        }
    }
}
