package tranlinh.rikkeisofttrainning.com.widgeteventhandling;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    Button btnGoodbye;
    TextView tv;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.connectWithId();
        btnGoodbye.setOnClickListener(new onClickButton());
    }
    private void connectWithId(){
        btnGoodbye = (Button)findViewById(R.id.btnGoodbye);
        tv = (TextView)findViewById(R.id.tv);
    }
    public void onClick(View view){
        Toast.makeText(MainActivity.this,((Button)view).getText(),Toast.LENGTH_SHORT).show();
        tv.setText("");
    }
    private class onClickButton implements View.OnClickListener{

        @Override
        public void onClick(View v) {
            tv.setText(((Button) v).getText());
        }
    }
}
