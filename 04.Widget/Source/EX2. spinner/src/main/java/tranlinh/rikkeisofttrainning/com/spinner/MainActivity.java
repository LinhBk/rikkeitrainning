package tranlinh.rikkeisofttrainning.com.spinner;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private TextView tv;
    private Spinner sp,sp2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.connectWithId();

        //spinner 1
        sp.setOnItemSelectedListener(new clickOnSpinner());

        //spinner2
        List<String> spinnerEntries = getEntriesArray();
        ArrayAdapter<String> spinner2Adapter = new ArrayAdapter<String>(MainActivity.this, R.layout.support_simple_spinner_dropdown_item,spinnerEntries);
        spinner2Adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        sp2.setAdapter(spinner2Adapter);
        sp2.setOnItemSelectedListener(new clickOnSpinner());
    }

    private void connectWithId(){
        sp = (Spinner)findViewById(R.id.spinner1);
        sp2 = (Spinner)findViewById(R.id.spinner2);
        tv = (TextView)findViewById(R.id.tv);
    }
    private void changeTvBackgroundColor(int colorId){
        tv.setBackgroundColor(colorId);
    }
    private void setColor(String str){
        str = str.toUpperCase();
        changeTvBackgroundColor(Color.parseColor(str));
    }
    private class clickOnSpinner implements AdapterView.OnItemSelectedListener{
        private boolean isFirst = true;

        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            if(isFirst){
                isFirst = false;
            }
            else {
                String str = parent.getItemAtPosition(position).toString();
                setColor(str);
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {
            //
        }
    }
    private List<String> getEntriesArray(){
        String[] array = {"Blue","Red","Yellow","Green","Black","White"};
        List<String> stringList = Arrays.asList(array);
        Collections.shuffle(stringList);
        return stringList;
    }
}
