package tranlinh.rikkeisofttrainning.com.intentdemo1;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

/**
 * Created by Tran Linh on 7/20/2016.
 */
public class ActivitySum extends Activity {
    private TextView textView;
    private Button button;
    private Number number;
    private int flag = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sum);
                                                                                                    //lay ra gia tri truyen, thong qua bundle
        if(this.getBundle()==1){
            if(this.getData() ==1){
                number = new Number();
            }
        }

                                                                                                    // gan thong bao ve tong 2 so cho textview
        String message = getString(R.string.sum_promt);
        message = String.format(message,String.valueOf(number.getFirstNumber()),String.valueOf(number.getSecondNumber()),String.valueOf(number.getSum()));
        textView = (TextView)findViewById(R.id.tv3);
        textView.setText(message);
                                                                                                    // xu ly nut back
        button = (Button)findViewById(R.id.btn4);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }
    private int getBundle(){
        Intent intent = getIntent();
        Bundle bundle = intent.getBundleExtra("bundle_1");
        if(bundle != null){
            number = (Number)bundle.getSerializable("1");                                           // lay duoc bundle, lay ra doi tuong Number
            return 2;
        }
        return 1;
    }
    private int getData(){
        Uri uri = getIntent().getData();
        if(uri!=null) {
            //number = new Number(getDoubleParam(uri, "number1"), getDoubleParam(uri, "number2"));
            Double number1 = getDoubleParam(uri, "number1");
            Double number2 = getDoubleParam(uri, "number2");
            if (number1 != 0 && number2 != 0) {
                number = new Number(number1, number2);
                return 3;
            }
        }
        return 1;
    }
    private double getDoubleParam(Uri uri, String queryParamName){
        String rawValue = uri.getQueryParameter(queryParamName);
        double value = 0.0;
        try{
            value = Double.parseDouble(rawValue);
        }catch (Exception e){}
        return value;
    }
}
