package tranlinh.rikkeisofttrainning.com.intentdemo1;

import android.app.TabActivity;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.widget.TabHost;

/**
 * Created by Tran Linh on 7/26/2016.
 */
public class TabbedWindow extends TabActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        Resources resources = getResources();
        TabHost host = getTabHost();
        Intent intent= new Intent(this,ActivitySum.class);
        //Tab 1
        TabHost.TabSpec tabSpec = host.newTabSpec("tab1").setIndicator("by class name, no data").setContent(intent);
        host.addTab(tabSpec);
        //Tab 2
        Number number = new Number(Math.random(),Math.random());
        Bundle bundle = new Bundle();
        bundle.putSerializable("1",number);
        intent.putExtra("bundle_1", bundle);
        TabHost.TabSpec tabSpec1 = host.newTabSpec("tab2").setIndicator("by class name, random data").setContent(intent);
        host.addTab(tabSpec1);
        //Tab 3
        String address = "load://ActivitySum";
        Intent intent1 = new Intent(Intent.ACTION_VIEW, Uri.parse(address));
        TabHost.TabSpec tabSpec2 = host.newTabSpec("tab3").setIndicator("by Uri, no data").setContent(intent1);
        host.addTab(tabSpec2);
        //Tab 4
        String value1 = String.valueOf(Math.random());
        String value2 = String.valueOf(Math.random());
        String key1 = "number1";
        String key2 = "number2";
        address = String.format("%s?%s=%s&%s=%s",address,key1,value1,key2,value2);
        Intent intent2 = new Intent(Intent.ACTION_VIEW,Uri.parse(address));
        TabHost.TabSpec tabSpec3 = host.newTabSpec("tab4").setIndicator("by Uri, with data").setContent(intent2);
        host.addTab(tabSpec3);
    }
}
