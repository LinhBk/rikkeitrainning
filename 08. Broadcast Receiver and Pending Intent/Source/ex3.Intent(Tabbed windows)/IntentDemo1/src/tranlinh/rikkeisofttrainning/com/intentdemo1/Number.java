package tranlinh.rikkeisofttrainning.com.intentdemo1;

import java.io.Serializable;

/**
 * Created by Tran Linh on 7/20/2016.
 */
public class Number implements Serializable {
    private double firstNumber;
    private double secondNumber;
    public Number(){
        this.firstNumber = 5.0;
        this.secondNumber = 5.5;
    }
    public Number(double firstNumber,double secondNumber){
        this.firstNumber = firstNumber;
        this.secondNumber = secondNumber;
    }
    public double getFirstNumber(){
        return this.firstNumber;
    }
    public double getSecondNumber(){
        return this.secondNumber;
    }
    public double getSum(){
        return this.firstNumber + this.secondNumber;
    }
}
