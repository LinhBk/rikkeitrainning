package tranlinh.rikkeisofttrainning.com.broadcastreceiverdemo;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Intent intent = new Intent();
        intent.setAction("MyBroadcast");
        intent.putExtra("value","test custom broadcast receiver");
        sendBroadcast(intent);
    }
}
