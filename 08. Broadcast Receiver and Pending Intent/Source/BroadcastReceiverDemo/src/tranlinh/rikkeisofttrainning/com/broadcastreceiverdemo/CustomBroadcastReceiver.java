package tranlinh.rikkeisofttrainning.com.broadcastreceiverdemo;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

/**
 * Created by Tran Linh on 7/21/2016.
 */
public class CustomBroadcastReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Bundle bundle = intent.getExtras();
        if(bundle != null){
            if(bundle.getString("value")!=null){
                Toast.makeText(context,bundle.getString("value"),Toast.LENGTH_LONG).show();
            }
        }
    }
}
