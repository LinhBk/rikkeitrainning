package tranlinh.rikkeisofttrainning.com.broadcastreceiverdemo;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

/**
 * Created by Tran Linh on 7/21/2016.
 */
public class WifiChangeReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Toast.makeText(context,"Wifi state change - broadcast receiver detected",Toast.LENGTH_LONG).show();
    }
}
