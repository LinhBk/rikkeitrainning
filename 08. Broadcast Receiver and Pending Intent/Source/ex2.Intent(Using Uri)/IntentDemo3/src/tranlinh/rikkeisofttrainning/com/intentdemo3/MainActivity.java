package tranlinh.rikkeisofttrainning.com.intentdemo3;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
    public void CalculateWithDefaultValue(View v){
        Intent intent= new Intent(Intent.ACTION_VIEW, Uri.parse("load://ActivitySum"));
        startActivity(intent);
    }
    public void CalculateWithInputValue(View v){
        EditText edNumber1 = (EditText)findViewById(R.id.edNumber1);
        EditText edNumber2 = (EditText)findViewById(R.id.edNumber2);
        String value1 = String.valueOf(edNumber1.getText());
        String value2 = String.valueOf(edNumber2.getText());
        this.sendDataUsingUri(value1, value2);
    }
    public void CalculateWithRandomValue(View v){
        String value1 = String.valueOf(Math.random());
        String value2 = String.valueOf(Math.random());
        this.sendDataUsingUri(value1,value2);
    }
    private void sendDataUsingUri(String value1, String value2){
        String key1 = "number1";
        String key2 = "number2";
        String address = "load://ActivitySum/sendvalue";
        address = String.format("%s?%s=%s&%s=%s",address,key1,value1,key2,value2);
        Intent intent = new Intent(Intent.ACTION_VIEW,Uri.parse(address));
        startActivity(intent);
    }
}
