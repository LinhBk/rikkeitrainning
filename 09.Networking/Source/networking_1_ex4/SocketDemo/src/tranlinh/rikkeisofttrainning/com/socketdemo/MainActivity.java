package tranlinh.rikkeisofttrainning.com.socketdemo;

import android.app.Activity;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

public class MainActivity extends Activity {
    private TextView tv;
    private String host = "whois.verisign-grs.com";
    private int port = 43;
    private EditText edHostName;
    Socket socket;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tv = (TextView)findViewById(R.id.tv);
        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
        edHostName =(EditText)findViewById(R.id.edHostName);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void ShowInfomation(View view) {
        try {
            int totalLine =0;
            int flag =0;
            String result="";
            socket = new Socket(host,port);
            PrintWriter out = SocketUtils.getWriter(socket);
            BufferedReader in = SocketUtils.getReader(socket);
            out.println(edHostName.getText().toString());
            in.readLine();
            String line;
            result += in.readLine();
            while((line = in.readLine())!=null){
                if(line.startsWith(" ")){
                    if(flag ==0){
                        flag =1;
                        result += "\n";
                    }
                    result+="\n";
                }
                result += "@@"+line;
                totalLine++;
            }
            result = result.replace("@@@@","\n\n");
            result = result.replace("@@"," ");
            result += "\n Total "+String.valueOf(totalLine)+" lines";
            tv.setText(result.toString());
        } catch (UnknownHostException e){
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    private int checkLine(String line){
        Log.e("dawldkawl;dkaw;","dalwdk;awdklaw");
        if(line.startsWith(" ")){
            return 1;
        }
        if(line.startsWith("\n")){
            return 0;
        }
        if(line.charAt(0)>=65 && line.charAt(0)<=90){
            return 2;
        }
        return 0;
    }
}

class SocketUtils {
    public static BufferedReader getReader(Socket s) throws IOException {
        return(new BufferedReader
                (new InputStreamReader(s.getInputStream())));
    }
    public static PrintWriter getWriter(Socket s) throws IOException {
// Second argument of true means autoflush.
        return (new PrintWriter(s.getOutputStream(), true));
    }
}
