package tranlinh.rikkeisofttrainning.com.ex1;

import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class MainActivity extends AppCompatActivity {
    private TextView tvDisplay;
    private EditText edUrl;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tvDisplay = (TextView)findViewById(R.id.tvDisplay);
        edUrl = (EditText)findViewById(R.id.edUrl);
        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
    }

    public void Run(View view) {
        int lineNum=0;
        int charactorCount = 0;
        HttpURLConnection httpURLConnection = null;
        try{
            URL url = new URL(edUrl.getText().toString());
            httpURLConnection = (HttpURLConnection)url.openConnection();
            BufferedReader in = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream()));
            String line;
            while((line=in.readLine())!= null){
                lineNum++;
                charactorCount = charactorCount+line.length();
            }
            tvDisplay.setText("Number of lines in the page : "+String.valueOf(lineNum)+"\nNumber of charactor : "+ charactorCount);
        } catch (MalformedURLException e){
            tvDisplay.setText("bad URL:" + e);
            e.printStackTrace();
        } catch (IOException e){
            tvDisplay.setText("IOException "+e);
            e.printStackTrace();
        }finally {
            if(httpURLConnection!=null){
                httpURLConnection.disconnect();
            }
        }
    }
}
